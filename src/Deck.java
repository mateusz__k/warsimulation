import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class Deck {
    private LinkedList<Integer> deck;


    public Deck() {
        deck = new LinkedList<Integer>() {
        };

        for (int x = 1; x <= 4; x++) {
            for (int i = 2; i <= 14; i++) {
                deck.add(i);

            }
        }
        Collections.shuffle(deck);
    }

    public LinkedList<Integer> getDeck() {
        return deck;
    }


    public LinkedList<Integer> halfDeck(LinkedList<Integer> source, LinkedList<Integer> destanation) {
        for(int i=0; i <= 25; i++) {
            destanation.add(source.remove(i));
        }
        return destanation;
    }


}
