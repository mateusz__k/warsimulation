import java.util.ArrayList;
import java.util.LinkedList;

public class Player {
    private LinkedList<Integer> playerDeck = new LinkedList<>();


    public Player() {
        this.playerDeck = playerDeck;
    }


    public LinkedList<Integer> getPlayerDeck() {
        return playerDeck;
    }


    public int getCard(int card) {
        return getPlayerDeck().get(card);
    }


    public void add(int card) {
        playerDeck.add(card);
    }


    public void remove(int card) {
        playerDeck.remove(card);
    }


    public Integer getFirst() {
        return getPlayerDeck().getFirst();
    }
}