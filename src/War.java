public class War{private static void war(Player player1, Player player2, int i) {
        int p1card1 = player1.getDeck1().get(i);
        int p1card2 = player1.getDeck1().get(i + 1);
        int p1card3 = player1.getDeck1().get(i + 2);
        int finalCard1 = player1.getDeck1().get(i + 3);

        int p2card1 = player2.getDeck2().get(i);
        int p2card2 = player2.getDeck2().get(i + 1);
        int p2card3 = player2.getDeck2().get(i + 2);
        int finalCard2 = player2.getDeck2().get(i + 3);
        if (finalCard1 > finalCard2) {
            player1.getDeck1().add(p2card1);
            player1.getDeck1().add(p2card2);
            player1.getDeck1().add(p2card3);
            player1.getDeck1().add(finalCard2);
            player2.getDeck2().remove(p2card1);
            player2.getDeck2().remove(p2card2);
            player2.getDeck2().remove(p2card3);
            player2.getDeck2().remove(finalCard2);
        } else if (finalCard1 < finalCard2) {
            player2.getDeck2().add(p1card1);
            player2.getDeck2().add(p1card2);
            player2.getDeck2().add(p1card3);
            player2.getDeck2().add(finalCard1);
            player1.getDeck1().remove(p1card1);
            player1.getDeck1().remove(p1card2);
            player1.getDeck1().remove(p1card3);
            player1.getDeck1().remove(finalCard1);
        } else if (finalCard1 == finalCard2) {
            war(player1, player2, i);
        }
    }}